from django.urls import path

from .views import MovieDetailView, MovieList

urlpatterns = [
    path("api/movies/", MovieList.as_view()),
    path("api/movies/<int:pk>/", MovieDetailView.as_view()),
]
