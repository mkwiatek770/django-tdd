from rest_framework import serializers

from .models import Movie


# https://testdriven.io/blog/drf-serializers/
class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = "__all__"
        read_only_fields = (
            "id",
            "created_date",
            "updated_date",
        )
