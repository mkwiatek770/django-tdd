from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


# AbstractUser: Use this option if you are happy with the
# existing fields on the User model and just want to remove the username field.
# AbstractBaseUser: Use this option if you want to start from scratch by creating your own,
# completely new User model.
# https://testdriven.io/blog/django-custom-user-model/
class CustomUser(AbstractUser):
    pass


class Movie(models.Model):
    title = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    year = models.CharField(max_length=4)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title
